package scalemanager

import (
	scalemanagerapp "appautoscale/cmd/scalemanager/app"
	"appautoscale/common"
	"appautoscale/log"
	"appautoscale/pkg/monitor/metricplugin"
	"appautoscale/pkg/scalemanager/scaleplugin"
	"fmt"

	restclient "k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/kubernetes/pkg/client/clientset_generated/clientset"
)

func createClient(sma *scalemanagerapp.ScaleManagerConfig) (*clientset.Clientset, error) {
	kubeconfig, err := clientcmd.BuildConfigFromFlags("", sma.Kubeconfig)
	if err != nil {
		return nil, fmt.Errorf("unable to build config from flags: %v", err)
	}

	kubeconfig.ContentType = sma.ContentType

	cli, err := clientset.NewForConfig(restclient.AddUserAgent(kubeconfig, "enndata-clustermanager"))
	if err != nil {
		return nil, fmt.Errorf("invalid API configuration: %v", err)
	}
	return cli, nil
}

func Run(sma *scalemanagerapp.ScaleManagerConfig) error {
	if err := run(sma); err != nil {
		return fmt.Errorf("failed to run ScaleManagerServer : %v", err)
	}
	return nil
}

func checkConfig(sma *scalemanagerapp.ScaleManagerConfig) error {
	filePathCheck := func(name, path string) error {
		if path == "" {
			return fmt.Errorf("%s is empty", name)
		}
		if common.IsHostPathExist(path) == false {
			return fmt.Errorf("%s path %s is not exist", name, path)
		}
		return nil
	}
	if sma.HttpsAddr == "" {
		return fmt.Errorf("httpsaddr should not be empty")
	}
	if err := filePathCheck("kubeconfig", sma.Kubeconfig); err != nil {
		return err
	}
	if err := filePathCheck("tokenfilepath", sma.TokenFilePath); err != nil {
		return err
	}
	if err := filePathCheck("certfilepath", sma.CertFilePath); err != nil {
		return err
	}
	if err := filePathCheck("keyfilepath", sma.KeyFilePath); err != nil {
		return err
	}
	return nil
}
func run(sma *scalemanagerapp.ScaleManagerConfig) error {
	log.MyLogI("start run ScaleManagerServer")
	if err := checkConfig(sma); err != nil {
		return err
	}
	client, errCreateClient := createClient(sma)
	if errCreateClient != nil {
		return errCreateClient
	}
	scaleManager := scaleplugin.NewScaleManager(sma.ScalePlugins, client, metricplugin.NewMetricManager(sma.MetricPlugins))
	return installHttpServer(sma.HttpsAddr, sma.TokenFilePath, sma.CertFilePath, sma.KeyFilePath, scaleManager)
}
