package replicationcontrollerset

import (
	"appautoscale/common"
	"appautoscale/errors"
	"appautoscale/log"
	"appautoscale/pkg/monitor/metricplugin"
	"appautoscale/pkg/scalemanager/scaleplugin"
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/kubernetes/pkg/api"
	"k8s.io/kubernetes/pkg/api/v1"
	"k8s.io/kubernetes/pkg/client/clientset_generated/clientset"
)

const (
	pluginName                       string = "ReplicationControllerSet"
	replicationControllerSetIndexAnn string = "io.enndata.appautoscale/replicationcontroller_index"
	replicationControllerSetSortAnn  string = "io.enndata.appautoscale/replicationcontroller_sort"
)

func ProbeVolumePlugins() []scaleplugin.ScalePlugin {
	return []scaleplugin.ScalePlugin{
		&replicationControllerSet{},
	}
}

var _ scaleplugin.ScalePlugin = &replicationControllerSet{}
var _ scaleplugin.Scale = &replicationControllerSetScale{}

type replicationControllerSetDeleteSort struct {
	rcs            []*v1.ReplicationController
	scoresMap      map[string]float64
	pods           []*v1.Pod
	mm             *metricplugin.MetricManager
	moniterMetrics []common.MoniterMetric
	metricSorts    []common.MetricSort
}

func (rcds *replicationControllerSetDeleteSort) getScore(i int) float64 {
	if score, exist := rcds.scoresMap[rcds.rcs[i].Name]; exist == true {
		return score
	} else {
		if rcds.pods == nil || len(rcds.pods) == 0 {
			log.MyLogD("getScore of %d return1", i)
			return 0.0
		}
		pods, err := getReplicationControllerPods(rcds.rcs[i], rcds.pods)
		if err != nil {
			log.MyLogD("getScore of %d return2: err:%v", i, err)
			return 0.0
		}
		moniterMetricMap := make(map[string]common.MoniterMetric)
		for _, mm := range rcds.moniterMetrics {
			moniterMetricMap[mm.Name] = mm
		}
		var metricScoreSum float64
		for _, metric := range rcds.metricSorts {
			if mm, exist := moniterMetricMap[metric.MetricName]; exist == false {
				log.MyLogD("getScore of %d return3: %s not exist", i, metric.MetricName)
				return 0.0
			} else {
				plugin, err := rcds.mm.FindPlugin(&mm.MetricGeter)
				if err != nil {
					log.MyLogD("getScore of %d return4: %s plugin not exist", i, metric.MetricName)
					return 0.0
				}
				getterList := make([]metricplugin.MetricGeter, 0, len(pods))
				for _, pod := range pods {
					getter, errNew := plugin.NewMetricGeter(&mm, pod)
					if errNew != nil {
						log.MyLogD("getScore of %d return4: %s plugin NewMetricGeter err:%v", i, metric.MetricName, errNew)
						return 0.0
					}
					getterList = append(getterList, getter)
				}
				mgl := &metricplugin.MetricGeterList{
					MetricName:     mm.Name,
					Getters:        getterList,
					Duration:       mm.MetricsTime,
					UpScaleValue:   mm.UpScaleValue,
					DownScaleValue: mm.DownScaleValue,
				}
				score, errGetScore := mgl.GetMetricScore()
				if errGetScore == nil {
					metricScoreSum += score * float64(metric.Weight)
				}
			}
		}
		rcds.scoresMap[rcds.rcs[i].Name] = metricScoreSum
		return metricScoreSum
	}
}

func (rcds *replicationControllerSetDeleteSort) Len() int {
	return len(rcds.rcs)
}
func (rcds *replicationControllerSetDeleteSort) Swap(i, j int) {
	rcds.rcs[i], rcds.rcs[j] = rcds.rcs[j], rcds.rcs[i]
}

func (rcds *replicationControllerSetDeleteSort) Less(i, j int) bool {
	if rcds.rcs[i].Status.AvailableReplicas != rcds.rcs[j].Status.AvailableReplicas {
		return rcds.rcs[i].Status.AvailableReplicas < rcds.rcs[j].Status.AvailableReplicas
	}
	ri := *rcds.rcs[i].Spec.Replicas
	rj := *rcds.rcs[j].Spec.Replicas
	if ri == 0 {
		return true
	}
	if rj == 0 {
		return false
	}
	wi := getReplicationControllerSortWeight(rcds.rcs[i])
	wj := getReplicationControllerSortWeight(rcds.rcs[j])
	if wi != wj {
		return wi > wj
	}
	scorei := rcds.getScore(i)
	scorej := rcds.getScore(j)
	if common.IsFloatEqual(scorei, scorej) != true {
		return scorei < scorej
	}
	return getReplicationControllerIndex(rcds.rcs[i]) > getReplicationControllerIndex(rcds.rcs[j])
}

type replicationControllerSet struct {
	client *clientset.Clientset
	mm     *metricplugin.MetricManager
}

func (rcs *replicationControllerSet) PluginName() string {
	return pluginName
}

func (rcs *replicationControllerSet) Init(client *clientset.Clientset, mm *metricplugin.MetricManager) error {
	if client == nil {
		return fmt.Errorf("replicationControllerSet int client == nil")
	}
	rcs.client = client
	rcs.mm = mm
	return nil
}

func (rcs *replicationControllerSet) NewScale(sm *common.ScaleMoniter) (scaleplugin.Scale, error) {
	if sm.ScaleTemplate.ReplicationControllerSet == nil {
		return nil, fmt.Errorf("ScaleTemplate.ReplicationControllerSet is nil")
	}

	return &replicationControllerSetScale{
		rc:               sm.ScaleTemplate.ReplicationControllerSet,
		client:           rcs.client,
		mm:               rcs.mm,
		canDeleteTrigger: sm.CanDeleteTrigger,
		moniterMetrics:   sm.MoniterMetrics,
		metricSorts:      sm.SvcMetricSort,
		maxReplica:       sm.MaxReplics,
		minReplica:       sm.MinReplics,
		svcPool:          scaleplugin.NewServicePool(sm.ScaleTemplate.ReplicationControllerSet.Namespace, sm.SvcPool, rcs.client),
	}, nil
}

type replicationControllerSetScale struct {
	rc                     *v1.ReplicationController
	canDeleteTrigger       string
	metricSorts            []common.MetricSort
	moniterMetrics         []common.MoniterMetric
	client                 *clientset.Clientset
	mm                     *metricplugin.MetricManager
	maxReplica, minReplica int
	svcPool                *scaleplugin.ServicePool
}

func getReplicationControllerIndex(rc *v1.ReplicationController) int {
	if rc == nil || rc.Annotations == nil || rc.Annotations[replicationControllerSetIndexAnn] == "" {
		return -1
	}
	indexStr := rc.Annotations[replicationControllerSetIndexAnn]
	if index, err := strconv.Atoi(indexStr); err != nil {
		return -1
	} else {
		return index
	}
}
func getReplicationControllerSortWeight(rc *v1.ReplicationController) int {
	if rc == nil || rc.Annotations == nil || rc.Annotations[replicationControllerSetSortAnn] == "" {
		return 0
	}
	weightStr := rc.Annotations[replicationControllerSetSortAnn]
	if weight, err := strconv.Atoi(weightStr); err != nil {
		return 0
	} else {
		return weight
	}
}
func (rcs *replicationControllerSetScale) getPods() ([]*v1.Pod, error) {
	podList, err := rcs.client.CoreV1().Pods(rcs.rc.Namespace).List(metav1.ListOptions{})
	if err != nil {
		return []*v1.Pod{}, fmt.Errorf("get pod from ns %s err:%v", rcs.rc.Namespace, err)
	}
	ret := make([]*v1.Pod, 0, len(podList.Items))
	for i := range podList.Items {
		ret = append(ret, &podList.Items[i])
	}
	return ret, nil
}

func (rcs *replicationControllerSetScale) isPodCanBeDeleted(pod *v1.Pod) bool {
	if rcs.canDeleteTrigger == "" { // no define canDeleteTrigger all pod can be deleted
		log.MyLogD("canDeleteTrigger is empty all pod can be deleted")
		return true
	}
	if common.IsReadyPod(pod) == false {
		return true
	}
	mgls := make([]*metricplugin.MetricGeterList, 0, len(rcs.moniterMetrics))

	for _, metric := range rcs.moniterMetrics {
		plugin, err := rcs.mm.FindPlugin(&metric.MetricGeter)
		if err != nil {
			log.MyLogE("metric %s cann't find plugin", metric.Name)
			return false
		}
		getter, errNew := plugin.NewMetricGeter(&metric, pod)
		if errNew != nil {
			log.MyLogE("plugin %s NewMetricGeter for pod %s:%s err:%v", plugin.PluginName(), pod.Namespace, pod.Name, errNew)
			return false
		}
		mgl := &metricplugin.MetricGeterList{
			MetricName:     metric.Name,
			Getters:        []metricplugin.MetricGeter{getter},
			Duration:       metric.MetricsTime,
			UpScaleValue:   metric.UpScaleValue,
			DownScaleValue: metric.DownScaleValue,
		}
		mgls = append(mgls, mgl)
	}
	mt := &metricplugin.MetricTrigger{
		Mgls:        mgls,
		Downtrigger: rcs.canDeleteTrigger,
	}
	//log.MyLogD("pod:%s:%s trigger:%s, len(mgls)=%d", pod.Namespace, pod.Name, mt.Downtrigger, len(mt.Mgls))
	if ok, errDown := mt.IsDown(); ok == true && errDown == nil {
		return true
	} else if ok == false && errDown == nil {
		log.MyLogD("pod %s:%s cann't be delete because trigger:%s", pod.Namespace, pod.Name, rcs.canDeleteTrigger)
	} else {
		log.MyLogE("pod %s:%s cann't be delete bacause check err:%v", pod.Namespace, pod.Name, errDown)
	}
	return false
}

func getReplicationControllerPods(rc *v1.ReplicationController, podList []*v1.Pod) ([]*v1.Pod, error) {
	selector, err := metav1.LabelSelectorAsSelector(&metav1.LabelSelector{MatchLabels: rc.Spec.Selector})
	if err != nil {
		return []*v1.Pod{}, err
	}
	ret := make([]*v1.Pod, 0, len(podList))
	for _, pod := range podList {
		labels := labels.Set(pod.Labels)
		if selector.Matches(labels) {
			ret = append(ret, pod)
		}
	}
	return ret, nil
}

func (rcs *replicationControllerSetScale) isReplicationControllerCanDelete(rc *v1.ReplicationController, podList []*v1.Pod) bool {
	pods, err := getReplicationControllerPods(rc, podList)
	if err != nil {
		log.MyLogE("get pods of repliationcontroller %s:%s err:%v", rc.Namespace, rc.Name, err)
		return false
	}
	log.MyLogD("isReplicationControllerCanDelete podlist:%d, pods:%d of repliaset:%s:%s", len(podList), len(pods), rc.Namespace, rc.Name)
	for _, pod := range pods {
		if rcs.isPodCanBeDeleted(pod) == false {
			return false
		}
	}
	return true
}
func (rcs *replicationControllerSetScale) fileterCanDeleteReplicationcontroller(list []*v1.ReplicationController) []*v1.ReplicationController {
	pods, err := rcs.getPods()
	if err != nil {
		log.MyLogE("fileterCanDeleteReplicationcontroller getPods err:%v", err)
		return []*v1.ReplicationController{}
	}
	ret := make([]*v1.ReplicationController, 0, len(list))
	for _, r := range list {
		if rcs.isReplicationControllerCanDelete(r, pods) == true {
			ret = append(ret, r)
		}
	}
	return ret
}

func (rcs *replicationControllerSetScale) Clean() error {
	return rcs.scale(0, nil)
}

func (rcs *replicationControllerSetScale) ScaleTo(replica int) error {
	if replica > rcs.maxReplica || replica < rcs.minReplica {
		return fmt.Errorf("cannot scale to %d no in [%d, %d]", replica, rcs.minReplica, rcs.maxReplica)
	}
	return rcs.scale(replica, nil)
}

func (rcs *replicationControllerSetScale) WaitScaleTo(replica int, timeout time.Duration) error {
	if replica > rcs.maxReplica || replica < rcs.minReplica {
		return fmt.Errorf("cannot scale to %d no in [%d, %d]", replica, rcs.minReplica, rcs.maxReplica)
	}
	return rcs.scale(replica, &timeout)
}

func (rcs *replicationControllerSetScale) getAndFilterReplicationControllers() ([]*v1.ReplicationController, error) {
	rcList, errList := rcs.client.CoreV1().ReplicationControllers(rcs.rc.Namespace).List(metav1.ListOptions{})
	if errList != nil {
		return []*v1.ReplicationController{}, fmt.Errorf("fileterReplicationControllers list replicas of %s namespace :%v", rcs.rc.Namespace, errList)
	}

	return rcs.fileterReplicationControllers(rcList.Items)
}

func (rcs *replicationControllerSetScale) fileterReplicationControllers(list []v1.ReplicationController) ([]*v1.ReplicationController, error) {
	selector, err := metav1.LabelSelectorAsSelector(&metav1.LabelSelector{MatchLabels: rcs.rc.Spec.Template.Labels})
	if err != nil {
		return []*v1.ReplicationController{}, err
	}
	ret := make([]*v1.ReplicationController, 0, len(list))
	for i := range list {
		labels := labels.Set(list[i].Labels)
		if selector.Matches(labels) && getReplicationControllerIndex(&list[i]) > 0 {
			ret = append(ret, &list[i])
		}
	}
	return ret, nil
}
func (rcs *replicationControllerSetScale) IsScaling() (bool, error) {
	filterList, errFilter := rcs.getAndFilterReplicationControllers()
	if errFilter != nil {
		return false, fmt.Errorf("getAndFilterReplicationControllers err:%v", errFilter)
	}
	for _, rc := range filterList {
		replica := rc.Status.Replicas
		if rc.Spec.Replicas != nil {
			replica = *rc.Spec.Replicas
		}
		if rc.Status.AvailableReplicas != replica {
			return true, nil
		}
	}
	return false, nil
}

func (rcs *replicationControllerSetScale) GetCurReplica() (cur, wanted int, err error) {
	filterList, errFilter := rcs.getAndFilterReplicationControllers()
	if errFilter != nil {
		return -1, -1, fmt.Errorf("getAndFilterReplicationControllers err:%v", errFilter)
	}
	for _, rc := range filterList {
		replica := rc.Status.Replicas
		if rc.Spec.Replicas != nil {
			replica = *rc.Spec.Replicas
		}
		cur += int(rc.Status.AvailableReplicas)
		wanted += int(replica)
	}
	return
}

func mergeLabels(to map[string]string, addLabel map[string]string) error {
	for k, v := range addLabel {
		if existValue, exist := to[k]; exist && existValue != v {
			return fmt.Errorf("%s is exist valuse is %s, cann't set to %s", k, existValue, v)
		} else if exist == false {
			to[k] = v
		}
	}
	return nil
}

func (rcs *replicationControllerSetScale) genReplicationControllerByIndex(index int, addLabels map[string]string) (*v1.ReplicationController, error) {
	newObj, err := api.Scheme.DeepCopy(rcs.rc)
	if err != nil {
		return nil, err
	}
	rc := newObj.(*v1.ReplicationController)
	rc.Name = fmt.Sprintf("%s-%d", rc.Name, index)
	if rc.Annotations == nil {
		rc.Annotations = make(map[string]string)
	}
	rc.Annotations[replicationControllerSetIndexAnn] = strconv.Itoa(index)
	if rc.Spec.Template.Labels == nil {
		rc.Spec.Template.Labels = make(map[string]string)
	}
	rc.Spec.Template.Labels[common.ReplicationControllerIndexLabelName] = strconv.Itoa(index)
	errMerge1 := mergeLabels(rc.Spec.Template.Labels, addLabels)
	if errMerge1 != nil {
		return nil, fmt.Errorf("merge replciationcontroller %s Spec.Template.Labels err:%v", rc.Name, errMerge1)
	}
	if rc.Spec.Selector == nil {
		rc.Spec.Selector = make(map[string]string)
	}
	rc.Spec.Selector[common.ReplicationControllerIndexLabelName] = strconv.Itoa(index)
	errMerge2 := mergeLabels(rc.Spec.Selector, addLabels)
	if errMerge2 != nil {
		return nil, fmt.Errorf("merge replciaset %s Spec.Selector.MatchLabels err:%v", rc.Name, errMerge2)
	}
	return rc, nil
}

func (rcs *replicationControllerSetScale) deleteReplicationController(rc *v1.ReplicationController, timeOut time.Duration) error {
	rc, errGet := rcs.client.CoreV1().ReplicationControllers(rc.Namespace).Get(rc.Name, metav1.GetOptions{})
	if errGet != nil {
		return errGet
	}
	if *rc.Spec.Replicas != 0 {
		var size int32 = 0
		rc.Spec.Replicas = &size
		_, err := rcs.client.CoreV1().ReplicationControllers(rc.Namespace).Update(rc)
		if err != nil {
			return err
		}
	}

	waitFun := func() (bool, error) {
		rc, err := rcs.client.CoreV1().ReplicationControllers(rc.Namespace).Get(rc.Name, metav1.GetOptions{})
		if err != nil {
			return false, err
		}
		return rc.Status.AvailableReplicas == 0, nil
	}

	errWait := wait.Poll(time.Second, timeOut, waitFun)
	if errWait == wait.ErrWaitTimeout {
		return fmt.Errorf("delete deployment %s wait UpdatedReplicas == 0 timeout", rc.Name)
	}
	if errWait != nil {
		return errWait
	}
	return rcs.client.CoreV1().ReplicationControllers(rc.Namespace).Delete(rc.Name, nil)
}

func (rcs *replicationControllerSetScale) scaleSimple(newSize int) (needTry bool, err error) {
	filterList, errFilter := rcs.getAndFilterReplicationControllers()
	if errFilter != nil {
		return true, fmt.Errorf("getAndFilterReplicationControllers err:%v", errFilter)
	}
	indexMap := make(map[int]bool)
	for _, d := range filterList {
		index := getReplicationControllerIndex(d)
		indexMap[index] = true
	}
	indexGet := func() int {
		for i := 1; i <= rcs.maxReplica; i++ {
			if exist := indexMap[i]; exist == false {
				return i
			}
		}
		return -1
	}
	if len(filterList) == newSize {
		return false, nil
	} else if len(filterList) < newSize {
		for i := len(filterList); i < newSize; i++ {
			index := indexGet()
			labels, errCG := rcs.svcPool.CreateAndGetServiceLabels(index)
			if errCG != nil {
				return true, fmt.Errorf("create svc %d err:%v", index, errCG)
			}
			rc, errGet := rcs.genReplicationControllerByIndex(index, labels)
			if errGet != nil {
				return true, fmt.Errorf("genReplicationControllerByIndex %d err:%v", index, errGet)
			}
			_, errCreate := rcs.client.CoreV1().ReplicationControllers(rc.Namespace).Create(rc)
			if errCreate != nil {
				return true, fmt.Errorf("create replicationcontroller %d err:%v", index, errCreate)
			}
			indexMap[index] = true
		}
	} else {
		deleteSize := len(filterList) - newSize
		canDeleteList := rcs.fileterCanDeleteReplicationcontroller(filterList)
		if deleteSize > len(canDeleteList) {
			deleteSize = len(canDeleteList)
		}
		pods, _ := rcs.getPods()
		rcds := &replicationControllerSetDeleteSort{
			rcs:            canDeleteList,
			scoresMap:      make(map[string]float64),
			pods:           pods,
			mm:             rcs.mm,
			moniterMetrics: rcs.moniterMetrics,
			metricSorts:    rcs.metricSorts,
		}
		sort.Sort(rcds)
		var wg sync.WaitGroup
		errstrs := make([]string, deleteSize)
		for i := 0; i < deleteSize; i++ {
			wg.Add(1)
			go func(rc *v1.ReplicationController, index int) {
				defer wg.Done()
				rcIndex := getReplicationControllerIndex(rc)
				errDeleteSvc := rcs.svcPool.DeleteSvc(rcIndex)
				if errDeleteSvc != nil {
					errstrs[index] = fmt.Sprintf("delete svc %d:%s", rcIndex, errDeleteSvc.Error())
					return
				}
				err := rcs.deleteReplicationController(rc, 10*time.Second)
				if err != nil {
					errstrs[index] = fmt.Sprintf("%d:%s", index, err.Error())
				} else {
					errstrs[index] = ""
				}
			}(rcds.rcs[i], i)
		}
		wg.Wait()
		strs := common.FilterEmptyString(errstrs)
		if len(strs) != 0 {
			return true, fmt.Errorf("scaleSimple err:[%s]", strings.Join(strs, ","))
		}
		if deleteSize != len(filterList)-newSize {
			return false,
				errors.NewScaleDeleteMetricNotMatch(
					fmt.Sprintf("%d replicationcontroller cann't be scale down because not match trigger %s",
						len(filterList)-newSize-deleteSize, rcs.canDeleteTrigger))

		}
	}
	return false, nil
}

func (rcs *replicationControllerSetScale) scale(newSize int, waitTimeOut *time.Duration) error {
	tryScale := func() (bool, error) {
		needTry, err := rcs.scaleSimple(newSize)
		if err != nil {
			log.MyLogE("scaleSimple err:%v", err)
			if needTry {
				return false, nil
			} else {
				return true, err
			}
		}
		return true, nil
	}

	if err := wait.Poll(100*time.Millisecond, 1*time.Minute, tryScale); err != nil {
		return err
	}

	if waitTimeOut != nil {
		filterList, errFilter := rcs.getAndFilterReplicationControllers()
		if errFilter != nil {
			return fmt.Errorf("getAndFilterReplicationControllers err:%v", errFilter)
		}
		waitMap := make(map[string]int64)
		for _, d := range filterList {
			waitMap[d.Name] = d.Generation
		}
		waitFun := func() (bool, error) {
			for name, generation := range waitMap {
				rc, err := rcs.client.CoreV1().ReplicationControllers(rcs.rc.Namespace).Get(name, metav1.GetOptions{})
				if err != nil {
					return false, err
				}
				replica := rc.Status.Replicas
				if rc.Spec.Replicas != nil {
					replica = *rc.Spec.Replicas
				}

				if rc.Status.ObservedGeneration >= generation && rc.Status.AvailableReplicas == replica {
					delete(waitMap, name)
				}
			}
			return true, nil
		}
		errWait := wait.Poll(time.Second, *waitTimeOut, waitFun)
		if errWait == wait.ErrWaitTimeout {
			return fmt.Errorf("timed out waiting replicationController %s synced", rcs.rc.Name)
		}
		return errWait
	}
	return nil
}
