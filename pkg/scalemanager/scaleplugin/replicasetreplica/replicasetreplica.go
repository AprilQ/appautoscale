package replicasetreplica

import (
	"appautoscale/common"
	"appautoscale/pkg/monitor/metricplugin"
	"appautoscale/pkg/scalemanager/scaleplugin"
	"fmt"
	"time"

	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/wait"
	"k8s.io/kubernetes/pkg/client/clientset_generated/clientset"
)

const (
	pluginName string = "ReplicaSetReplica"
)

func ProbeVolumePlugins() []scaleplugin.ScalePlugin {
	return []scaleplugin.ScalePlugin{
		&replicasetReplica{},
	}
}

var _ scaleplugin.ScalePlugin = &replicasetReplica{}
var _ scaleplugin.Scale = &replicasetReplicaScale{}

type replicasetReplica struct {
	client *clientset.Clientset
}

func (rr *replicasetReplica) PluginName() string {
	return pluginName
}

func (rr *replicasetReplica) Init(client *clientset.Clientset, mm *metricplugin.MetricManager) error {
	if client == nil {
		return fmt.Errorf("replicasetReplica int client == nil")
	}
	rr.client = client
	return nil
}

func (rr *replicasetReplica) NewScale(sm *common.ScaleMoniter) (scaleplugin.Scale, error) {
	if sm.ScaleTemplate.ReplicaSetReplica == nil {
		return nil, fmt.Errorf("ScaleTemplate.ReplicasetReplica is nil")
	}
	ns, name := common.GetNamespaceAndNameByKey(*sm.ScaleTemplate.ReplicaSetReplica)
	return &replicasetReplicaScale{
		namespace:  ns,
		name:       name,
		client:     rr.client,
		maxReplica: sm.MaxReplics,
		minReplica: sm.MinReplics,
	}, nil
}

type replicasetReplicaScale struct {
	namespace, name        string
	client                 *clientset.Clientset
	maxReplica, minReplica int
}

func (rrs *replicasetReplicaScale) Clean() error {
	//return rrs.scale(0, nil)
	return nil
}

func (rrs *replicasetReplicaScale) ScaleTo(replica int) error {
	if replica > rrs.maxReplica || replica < rrs.minReplica {
		return fmt.Errorf("cannot scale to %d no in [%d, %d]", replica, rrs.minReplica, rrs.maxReplica)
	}
	return rrs.scale(replica, nil)
}

func (rrs *replicasetReplicaScale) WaitScaleTo(replica int, timeout time.Duration) error {
	if replica > rrs.maxReplica || replica < rrs.minReplica {
		return fmt.Errorf("cannot scale to %d no in [%d, %d]", replica, rrs.minReplica, rrs.maxReplica)
	}
	return rrs.scale(replica, &timeout)
}

func (rrs *replicasetReplicaScale) IsScaling() (bool, error) {
	rs, errGet := rrs.client.Extensions().ReplicaSets(rrs.namespace).Get(rrs.name, metav1.GetOptions{})
	if errGet != nil {
		return false, fmt.Errorf("IsScaling replicaset %s/%s err get :%v", rrs.namespace, rrs.name, errGet)
	}
	replica := rs.Status.Replicas
	if rs.Spec.Replicas != nil {
		replica = *rs.Spec.Replicas
	}
	return rs.Status.AvailableReplicas != replica, nil
}

func (rrs *replicasetReplicaScale) GetCurReplica() (cur, wanted int, err error) {
	rs, errGet := rrs.client.Extensions().ReplicaSets(rrs.namespace).Get(rrs.name, metav1.GetOptions{})
	if errGet != nil {
		return -1, -1, fmt.Errorf("GetCurReplica replicaset %s/%s err get :%v", rrs.namespace, rrs.name, errGet)
	}
	replica := rs.Status.Replicas
	if rs.Spec.Replicas != nil {
		replica = *rs.Spec.Replicas
	}
	return int(rs.Status.AvailableReplicas), int(replica), nil
}

func (rrs *replicasetReplicaScale) scaleSimple(newSize int) error {
	rs, errGet := rrs.client.Extensions().ReplicaSets(rrs.namespace).Get(rrs.name, metav1.GetOptions{})
	if errGet != nil {
		return errGet
	}
	var size int32 = int32(newSize)
	rs.Spec.Replicas = &size
	_, err := rrs.client.Extensions().ReplicaSets(rrs.namespace).Update(rs)
	if err != nil {
		if errors.IsConflict(err) {
			return fmt.Errorf("update replicaset %s/%s conflict err:%v", rrs.namespace, rrs.name, err)
		}
		return fmt.Errorf("update replicaset %s/%s update err:%v", rrs.namespace, rrs.name, err)
	}
	return nil
}

func (rrs *replicasetReplicaScale) scale(newSize int, waitTimeOut *time.Duration) error {
	tryScale := func() (bool, error) {
		err := rrs.scaleSimple(newSize)
		if err != nil {
			if errors.IsNotFound(err) {
				return false, fmt.Errorf("replicaset %s:%s is not found", rrs.namespace, rrs.name)
			}
			return false, nil
		}
		return true, nil
	}

	if err := wait.Poll(100*time.Millisecond, 3*time.Second, tryScale); err != nil {
		return err
	}

	if waitTimeOut != nil {
		rs, err := rrs.client.Extensions().ReplicaSets(rrs.namespace).Get(rrs.name, metav1.GetOptions{})
		if err != nil {
			return err
		}
		desiredGeneration := rs.Generation
		waitFun := func() (bool, error) {
			rs, err := rrs.client.Extensions().ReplicaSets(rrs.namespace).Get(rrs.name, metav1.GetOptions{})
			if err != nil {
				return false, err
			}
			replica := rs.Status.Replicas
			if rs.Spec.Replicas != nil {
				replica = *rs.Spec.Replicas
			}

			return rs.Status.ObservedGeneration >= desiredGeneration &&
				rs.Status.AvailableReplicas == replica, nil
		}
		err = wait.Poll(time.Second, *waitTimeOut, waitFun)
		if err == wait.ErrWaitTimeout {
			return fmt.Errorf("timed out waiting for %q to be synced", rrs.name)
		}
		return err
	}
	return nil
}
