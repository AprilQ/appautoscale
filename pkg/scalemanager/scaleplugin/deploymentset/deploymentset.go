package deploymentset

import (
	"appautoscale/common"
	"appautoscale/errors"
	"appautoscale/log"
	"appautoscale/pkg/monitor/metricplugin"
	"appautoscale/pkg/scalemanager/scaleplugin"
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"k8s.io/kubernetes/pkg/api/v1"
	//"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/util/wait"
	extensions "k8s.io/kubernetes/pkg/apis/extensions/v1beta1"
	"k8s.io/kubernetes/pkg/client/clientset_generated/clientset"
	deploymentutil "k8s.io/kubernetes/pkg/controller/deployment/util"
)

const (
	pluginName         string = "DeploymentSet"
	deploymentIndexAnn string = "io.enndata.appautoscale/deploymentset_index"
	deploymentSortAnn  string = "io.enndata.appautoscale/deploymentset_sort"
)

func ProbeVolumePlugins() []scaleplugin.ScalePlugin {
	return []scaleplugin.ScalePlugin{
		&deploymentSet{},
	}
}

var _ scaleplugin.ScalePlugin = &deploymentSet{}
var _ scaleplugin.Scale = &deploymentSetScale{}

type DeploymentDeleteSort struct {
	deployments    []*extensions.Deployment
	scoresMap      map[string]float64
	pods           []*v1.Pod
	mm             *metricplugin.MetricManager
	moniterMetrics []common.MoniterMetric
	metricSorts    []common.MetricSort
}

func (dds *DeploymentDeleteSort) getScore(i int) float64 {
	if score, exist := dds.scoresMap[dds.deployments[i].Name]; exist == true {
		return score
	} else {
		if dds.pods == nil || len(dds.pods) == 0 {
			log.MyLogD("getScore of %d return1", i)
			return 0.0
		}
		pods, err := getDeployPods(dds.deployments[i], dds.pods)
		if err != nil {
			log.MyLogD("getScore of %d return2: err:%v", i, err)
			return 0.0
		}
		moniterMetricMap := make(map[string]common.MoniterMetric)
		for _, mm := range dds.moniterMetrics {
			moniterMetricMap[mm.Name] = mm
		}
		var metricScoreSum float64
		for _, metric := range dds.metricSorts {
			if mm, exist := moniterMetricMap[metric.MetricName]; exist == false {
				log.MyLogD("getScore of %d return3: %s not exist", i, metric.MetricName)
				return 0.0
			} else {
				plugin, err := dds.mm.FindPlugin(&mm.MetricGeter)
				if err != nil {
					log.MyLogD("getScore of %d return4: %s plugin not exist", i, metric.MetricName)
					return 0.0
				}
				getterList := make([]metricplugin.MetricGeter, 0, len(pods))
				for _, pod := range pods {
					getter, errNew := plugin.NewMetricGeter(&mm, pod)
					if errNew != nil {
						log.MyLogD("getScore of %d return4: %s plugin NewMetricGeter err:%v", i, metric.MetricName, errNew)
						return 0.0
					}
					getterList = append(getterList, getter)
				}
				mgl := &metricplugin.MetricGeterList{
					MetricName:     mm.Name,
					Getters:        getterList,
					Duration:       mm.MetricsTime,
					UpScaleValue:   mm.UpScaleValue,
					DownScaleValue: mm.DownScaleValue,
				}
				score, errGetScore := mgl.GetMetricScore()
				if errGetScore == nil {
					metricScoreSum += score * float64(metric.Weight)
				}
			}
		}
		dds.scoresMap[dds.deployments[i].Name] = metricScoreSum
		return metricScoreSum
	}
}

func (dds *DeploymentDeleteSort) Len() int {
	return len(dds.deployments)
}
func (dds *DeploymentDeleteSort) Swap(i, j int) {
	dds.deployments[i], dds.deployments[j] = dds.deployments[j], dds.deployments[i]
}

func (dds *DeploymentDeleteSort) Less(i, j int) bool {
	if dds.deployments[i].Status.UnavailableReplicas != dds.deployments[j].Status.UnavailableReplicas {
		return dds.deployments[i].Status.UnavailableReplicas > dds.deployments[j].Status.UnavailableReplicas
	}
	ri := *dds.deployments[i].Spec.Replicas
	rj := *dds.deployments[j].Spec.Replicas
	if ri == 0 {
		return true
	}
	if rj == 0 {
		return false
	}
	wi := getDeploymentSortWeight(dds.deployments[i])
	wj := getDeploymentSortWeight(dds.deployments[j])
	if wi != wj {
		return wi > wj
	}
	scorei := dds.getScore(i)
	scorej := dds.getScore(j)
	if common.IsFloatEqual(scorei, scorej) != true {
		return scorei < scorej
	}
	return getDeploymentIndex(dds.deployments[i]) > getDeploymentIndex(dds.deployments[j])
}

type deploymentSet struct {
	client *clientset.Clientset
	mm     *metricplugin.MetricManager
}

func (ds *deploymentSet) PluginName() string {
	return pluginName
}

func (ds *deploymentSet) Init(client *clientset.Clientset, mm *metricplugin.MetricManager) error {
	if client == nil {
		return fmt.Errorf("deploymentSet int client == nil")
	}
	ds.client = client
	ds.mm = mm
	return nil
}

func (ds *deploymentSet) NewScale( /*t *common.ScaleTemplate, svcPool *common.SvcPool, maxReplica, minReplica int,
	canDeleteTrigger string, moniterMetrics []common.MoniterMetric*/sm *common.ScaleMoniter) (scaleplugin.Scale, error) {
	if sm.ScaleTemplate.DeploymentSet == nil {
		return nil, fmt.Errorf("ScaleTemplate.DeploymentSet is nil")
	}

	return &deploymentSetScale{
		deploy:           sm.ScaleTemplate.DeploymentSet,
		client:           ds.client,
		mm:               ds.mm,
		canDeleteTrigger: sm.CanDeleteTrigger,
		moniterMetrics:   sm.MoniterMetrics,
		metricSorts:      sm.SvcMetricSort,
		maxReplica:       sm.MaxReplics,
		minReplica:       sm.MinReplics,
		svcPool:          scaleplugin.NewServicePool(sm.ScaleTemplate.DeploymentSet.Namespace, sm.SvcPool, ds.client),
	}, nil
}

type deploymentSetScale struct {
	deploy                 *extensions.Deployment
	canDeleteTrigger       string
	metricSorts            []common.MetricSort
	moniterMetrics         []common.MoniterMetric
	client                 *clientset.Clientset
	mm                     *metricplugin.MetricManager
	maxReplica, minReplica int
	svcPool                *scaleplugin.ServicePool
}

func getDeploymentIndex(deploy *extensions.Deployment) int {
	if deploy == nil || deploy.Annotations == nil || deploy.Annotations[deploymentIndexAnn] == "" {
		return -1
	}
	indexStr := deploy.Annotations[deploymentIndexAnn]
	if index, err := strconv.Atoi(indexStr); err != nil {
		return -1
	} else {
		return index
	}
}
func getDeploymentSortWeight(deploy *extensions.Deployment) int {
	if deploy == nil || deploy.Annotations == nil || deploy.Annotations[deploymentSortAnn] == "" {
		return 0
	}
	weightStr := deploy.Annotations[deploymentSortAnn]
	if weight, err := strconv.Atoi(weightStr); err != nil {
		return 0
	} else {
		return weight
	}
}
func (dss *deploymentSetScale) getPods() ([]*v1.Pod, error) {
	podList, err := dss.client.CoreV1().Pods(dss.deploy.Namespace).List(metav1.ListOptions{})
	if err != nil {
		return []*v1.Pod{}, fmt.Errorf("get pod from ns %s err:%v", dss.deploy.Namespace, err)
	}
	ret := make([]*v1.Pod, 0, len(podList.Items))
	for i := range podList.Items {
		ret = append(ret, &podList.Items[i])
	}
	return ret, nil
}

func (dss *deploymentSetScale) isPodCanBeDeleted(pod *v1.Pod) bool {
	if dss.canDeleteTrigger == "" { // no define canDeleteTrigger all pod can be deleted
		log.MyLogD("canDeleteTrigger is empty all pod can be deleted")
		return true
	}
	if common.IsReadyPod(pod) == false {
		return true
	}
	mgls := make([]*metricplugin.MetricGeterList, 0, len(dss.moniterMetrics))

	for _, metric := range dss.moniterMetrics {
		plugin, err := dss.mm.FindPlugin(&metric.MetricGeter)
		if err != nil {
			log.MyLogE("metric %s cann't find plugin", metric.Name)
			return false
		}
		getter, errNew := plugin.NewMetricGeter(&metric, pod)
		if errNew != nil {
			log.MyLogE("plugin %s NewMetricGeter for pod %s:%s err:%v", plugin.PluginName(), pod.Namespace, pod.Name, errNew)
			return false
		}
		mgl := &metricplugin.MetricGeterList{
			MetricName:     metric.Name,
			Getters:        []metricplugin.MetricGeter{getter},
			Duration:       metric.MetricsTime,
			UpScaleValue:   metric.UpScaleValue,
			DownScaleValue: metric.DownScaleValue,
		}
		mgls = append(mgls, mgl)
	}
	mt := &metricplugin.MetricTrigger{
		Mgls:        mgls,
		Downtrigger: dss.canDeleteTrigger,
	}
	//log.MyLogD("pod:%s:%s trigger:%s, len(mgls)=%d", pod.Namespace, pod.Name, mt.Downtrigger, len(mt.Mgls))
	if ok, errDown := mt.IsDown(); ok == true && errDown == nil {
		return true
	} else if ok == false && errDown == nil {
		log.MyLogD("pod %s:%s cann't be delete because trigger:%s", pod.Namespace, pod.Name, dss.canDeleteTrigger)
	} else {
		log.MyLogE("pod %s:%s cann't be delete bacause check err:%v", pod.Namespace, pod.Name, errDown)
	}
	return false
}

func getDeployPods(deploy *extensions.Deployment, podList []*v1.Pod) ([]*v1.Pod, error) {
	selector, err := metav1.LabelSelectorAsSelector(deploy.Spec.Selector)
	if err != nil {
		return []*v1.Pod{}, err
	}
	ret := make([]*v1.Pod, 0, len(podList))
	for _, pod := range podList {
		labels := labels.Set(pod.Labels)
		if selector.Matches(labels) {
			ret = append(ret, pod)
		}
	}
	return ret, nil
}

func (dss *deploymentSetScale) isDeploymentCanDelete(deploy *extensions.Deployment, podList []*v1.Pod) bool {
	pods, err := getDeployPods(deploy, podList)
	if err != nil {
		log.MyLogE("get pods of deploy %s:%s err:%v", deploy.Namespace, deploy.Name, err)
		return false
	}
	log.MyLogD("isDeploymentCanDelete podlist:%d, pods:%d of deploy:%s:%s", len(podList), len(pods), deploy.Namespace, deploy.Name)
	for _, pod := range pods {
		if dss.isPodCanBeDeleted(pod) == false {
			return false
		}
	}
	return true
}
func (dss *deploymentSetScale) fileterCanDeleteDeploy(list []*extensions.Deployment) []*extensions.Deployment {
	pods, err := dss.getPods()
	if err != nil {
		log.MyLogE("fileterCanDeleteDeploy getPods err:%v", err)
		return []*extensions.Deployment{}
	}
	ret := make([]*extensions.Deployment, 0, len(list))
	for _, d := range list {
		if dss.isDeploymentCanDelete(d, pods) == true {
			ret = append(ret, d)
		}
	}
	return ret
}
func (dss *deploymentSetScale) Clean() error {
	return dss.scale(0, nil)
}
func (dss *deploymentSetScale) ScaleTo(replica int) error {
	if replica > dss.maxReplica || replica < dss.minReplica {
		return fmt.Errorf("cannot scale to %d no in [%d, %d]", replica, dss.minReplica, dss.maxReplica)
	}
	return dss.scale(replica, nil)
}

func (dss *deploymentSetScale) WaitScaleTo(replica int, timeout time.Duration) error {
	if replica > dss.maxReplica || replica < dss.minReplica {
		return fmt.Errorf("cannot scale to %d no in [%d, %d]", replica, dss.minReplica, dss.maxReplica)
	}
	return dss.scale(replica, &timeout)
}

func (dss *deploymentSetScale) getAndFilterDeployments() ([]*extensions.Deployment, error) {
	deployList, errList := dss.client.Extensions().Deployments(dss.deploy.Namespace).List(metav1.ListOptions{})
	if errList != nil {
		return []*extensions.Deployment{}, fmt.Errorf("filterAndGetDeployments list deployment of %s namespace :%v", dss.deploy.Namespace, errList)
	}

	return dss.fileterDeploy(deployList.Items)
}

func (dss *deploymentSetScale) fileterDeploy(list []extensions.Deployment) ([]*extensions.Deployment, error) {
	selector, err := metav1.LabelSelectorAsSelector(&metav1.LabelSelector{MatchLabels: dss.deploy.Spec.Template.Labels})
	if err != nil {
		return []*extensions.Deployment{}, err
	}
	ret := make([]*extensions.Deployment, 0, len(list))
	for i := range list {
		labels := labels.Set(list[i].Labels)
		if selector.Matches(labels) && getDeploymentIndex(&list[i]) > 0 {
			ret = append(ret, &list[i])
		}
	}
	return ret, nil
}
func (dss *deploymentSetScale) IsScaling() (bool, error) {
	filterList, errFilter := dss.getAndFilterDeployments()
	if errFilter != nil {
		return false, fmt.Errorf("getAndFilterDeployments err:%v", errFilter)
	}
	for _, deploy := range filterList {
		replica := deploy.Status.Replicas
		if deploy.Spec.Replicas != nil {
			replica = *deploy.Spec.Replicas
		}
		if deploy.Status.UpdatedReplicas != replica {
			return true, nil
		}
	}
	return false, nil
}

func (dss *deploymentSetScale) GetCurReplica() (cur, wanted int, err error) {
	filterList, errFilter := dss.getAndFilterDeployments()
	if errFilter != nil {
		return -1, -1, fmt.Errorf("getAndFilterDeployments err:%v", errFilter)
	}
	for _, deploy := range filterList {
		replica := deploy.Status.Replicas
		if deploy.Spec.Replicas != nil {
			replica = *deploy.Spec.Replicas
		}
		cur += int(deploy.Status.UpdatedReplicas)
		wanted += int(replica)
	}
	return
}

func mergeLabels(to map[string]string, addLabel map[string]string) error {
	for k, v := range addLabel {
		if existValue, exist := to[k]; exist && existValue != v {
			return fmt.Errorf("%s is exist valuse is %s, cann't set to %s", k, existValue, v)
		} else if exist == false {
			to[k] = v
		}
	}
	return nil
}

func (dss *deploymentSetScale) genDeploymentByIndex(index int, addLabels map[string]string) (*extensions.Deployment, error) {
	d, err := deploymentutil.DeploymentDeepCopy(dss.deploy)
	if err != nil {
		return nil, err
	}
	d.Name = fmt.Sprintf("%s-%d", d.Name, index)
	if d.Annotations == nil {
		d.Annotations = make(map[string]string)
	}
	d.Annotations[deploymentIndexAnn] = strconv.Itoa(index)
	if d.Spec.Template.Labels == nil {
		d.Spec.Template.Labels = make(map[string]string)
	}
	d.Spec.Template.Labels[common.DeploymentIndexLabelName] = strconv.Itoa(index)
	errMerge1 := mergeLabels(d.Spec.Template.Labels, addLabels)
	if errMerge1 != nil {
		return nil, fmt.Errorf("merge deploy %s Spec.Template.Labels err:%v", d.Name, errMerge1)
	}
	if d.Spec.Selector == nil {
		d.Spec.Selector = &metav1.LabelSelector{}
	}
	if d.Spec.Selector.MatchLabels == nil {
		d.Spec.Selector.MatchLabels = make(map[string]string)
	}
	d.Spec.Selector.MatchLabels[common.DeploymentIndexLabelName] = strconv.Itoa(index)
	errMerge2 := mergeLabels(d.Spec.Selector.MatchLabels, addLabels)
	if errMerge2 != nil {
		return nil, fmt.Errorf("merge deploy %s Spec.Selector.MatchLabels err:%v", d.Name, errMerge2)
	}
	return d, nil
}

func (dss *deploymentSetScale) deleteDeployment(d *extensions.Deployment, timeOut time.Duration) error {
	deploy, errGet := dss.client.Extensions().Deployments(d.Namespace).Get(d.Name, metav1.GetOptions{})
	if errGet != nil {
		return errGet
	}
	if *deploy.Spec.Replicas != 0 {
		var size int32 = 0
		deploy.Spec.Replicas = &size
		_, err := dss.client.Extensions().Deployments(d.Namespace).Update(deploy)
		if err != nil {
			return err
		}
	}

	waitFun := func() (bool, error) {
		deployment, err := dss.client.Extensions().Deployments(deploy.Namespace).Get(deploy.Name, metav1.GetOptions{})
		if err != nil {
			return false, err
		}
		return deployment.Status.UpdatedReplicas == 0, nil
	}

	errWait := wait.Poll(time.Second, timeOut, waitFun)
	if errWait == wait.ErrWaitTimeout {
		return fmt.Errorf("delete deployment %s wait UpdatedReplicas == 0 timeout", deploy.Name)
	}
	if errWait != nil {
		return errWait
	}
	return dss.client.Extensions().Deployments(deploy.Namespace).Delete(deploy.Name, nil)
}

func (dss *deploymentSetScale) scaleSimple(newSize int) (needTry bool, err error) {
	filterList, errFilter := dss.getAndFilterDeployments()
	if errFilter != nil {
		return true, fmt.Errorf("getAndFilterDeployments err:%v", errFilter)
	}
	indexMap := make(map[int]bool)
	for _, d := range filterList {
		index := getDeploymentIndex(d)
		indexMap[index] = true
	}
	indexGet := func() int {
		for i := 1; i <= dss.maxReplica; i++ {
			if exist := indexMap[i]; exist == false {
				return i
			}
		}
		return -1
	}
	if len(filterList) == newSize {
		return false, nil
	} else if len(filterList) < newSize {
		for i := len(filterList); i < newSize; i++ {
			index := indexGet()
			labels, errCG := dss.svcPool.CreateAndGetServiceLabels(index)
			if errCG != nil {
				return true, fmt.Errorf("create svc %d err:%v", index, errCG)
			}
			deploy, errGet := dss.genDeploymentByIndex(index, labels)
			if errGet != nil {
				return true, fmt.Errorf("genDeploymentByIndex %d err:%v", index, errGet)
			}
			_, errCreate := dss.client.Extensions().Deployments(deploy.Namespace).Create(deploy)
			if errCreate != nil {
				return true, fmt.Errorf("create deploy %d err:%v", index, errCreate)
			}
			indexMap[index] = true
		}
	} else {
		deleteSize := len(filterList) - newSize
		canDeleteList := dss.fileterCanDeleteDeploy(filterList)
		if deleteSize > len(canDeleteList) {
			deleteSize = len(canDeleteList)
		}
		pods, _ := dss.getPods()
		dds := &DeploymentDeleteSort{
			deployments:    canDeleteList,
			scoresMap:      make(map[string]float64),
			pods:           pods,
			mm:             dss.mm,
			moniterMetrics: dss.moniterMetrics,
			metricSorts:    dss.metricSorts,
		}
		sort.Sort(dds)
		var wg sync.WaitGroup
		errstrs := make([]string, deleteSize)
		for i := 0; i < deleteSize; i++ {
			wg.Add(1)
			go func(d *extensions.Deployment, index int) {
				defer wg.Done()
				deployIndex := getDeploymentIndex(d)
				errDeleteSvc := dss.svcPool.DeleteSvc(deployIndex)
				if errDeleteSvc != nil {
					errstrs[index] = fmt.Sprintf("delete svc %d:%s", deployIndex, errDeleteSvc.Error())
					return
				}
				err := dss.deleteDeployment(d, 10*time.Second)
				if err != nil {
					errstrs[index] = fmt.Sprintf("%d:%s", index, err.Error())
				} else {
					errstrs[index] = ""
				}
			}(dds.deployments[i], i)
		}
		wg.Wait()
		strs := common.FilterEmptyString(errstrs)
		if len(strs) != 0 {
			return true, fmt.Errorf("scaleSimple err:[%s]", strings.Join(strs, ","))
		}
		if deleteSize != len(filterList)-newSize {
			return false,
				errors.NewScaleDeleteMetricNotMatch(
					fmt.Sprintf("%d deployment cann't be scale down because not match trigger %s",
						len(filterList)-newSize-deleteSize, dss.canDeleteTrigger))

		}
	}
	return false, nil
}

func (dss *deploymentSetScale) scale(newSize int, waitTimeOut *time.Duration) error {
	tryScale := func() (bool, error) {
		needTry, err := dss.scaleSimple(newSize)
		if err != nil {
			log.MyLogE("scaleSimple err:%v", err)
			if needTry {
				return false, nil
			} else {
				return true, err
			}
		}
		return true, nil
	}

	if err := wait.Poll(100*time.Millisecond, 1*time.Minute, tryScale); err != nil {
		return err
	}

	if waitTimeOut != nil {
		filterList, errFilter := dss.getAndFilterDeployments()
		if errFilter != nil {
			return fmt.Errorf("getAndFilterDeployments err:%v", errFilter)
		}
		waitMap := make(map[string]int64)
		for _, d := range filterList {
			waitMap[d.Name] = d.Generation
		}
		waitFun := func() (bool, error) {
			for name, generation := range waitMap {
				deployment, err := dss.client.Extensions().Deployments(dss.deploy.Namespace).Get(name, metav1.GetOptions{})
				if err != nil {
					return false, err
				}
				replica := deployment.Status.Replicas
				if deployment.Spec.Replicas != nil {
					replica = *deployment.Spec.Replicas
				}

				if deployment.Status.ObservedGeneration >= generation && deployment.Status.UpdatedReplicas == replica {
					delete(waitMap, name)
				}
			}
			return true, nil
		}
		errWait := wait.Poll(time.Second, *waitTimeOut, waitFun)
		if errWait == wait.ErrWaitTimeout {
			return fmt.Errorf("timed out waiting deploymentset %s synced", dss.deploy.Name)
		}
		return errWait
	}
	return nil
}
