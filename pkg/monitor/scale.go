package monitor

import (
	"appautoscale/common"
	"appautoscale/errors"
	"appautoscale/log"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
	"time"
)

type HttpScale struct {
	scaleServerAddr string
	username        string
	password        string
	client          *http.Client
}

func NewHttpScale(addr, username, password string) *HttpScale {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		Dial: func(netw, addr string) (net.Conn, error) {
			conn, err := net.DialTimeout(netw, addr, 30*time.Second) //设置建立连接超时
			if err != nil {
				return nil, err
			}
			conn.SetDeadline(time.Now().Add(30 * time.Second)) //设置发送接受数据超时
			return conn, nil
		},
	}
	return &HttpScale{
		scaleServerAddr: addr,
		username:        username,
		password:        password,
		client: &http.Client{
			Transport: tr,
		},
	}
}

type ScaleRet struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Err  string `json:"err"`
	Data string `json:"data"`
}

func (hs *HttpScale) GetCurReplica(namespace string, sm *common.ScaleMoniter) (cur, want int, err error) {
	key := common.GetKeyByNamespaceAndName(namespace, sm.Name)

	buf, jsonErr := json.Marshal(sm)
	if jsonErr != nil {
		return 0, 0, fmt.Errorf("Marshal ScaleMoniter %s err:%v", sm.Name, jsonErr)
	}
	addr := fmt.Sprintf("%s/replica", hs.scaleServerAddr)
	req, _ := http.NewRequest("POST", addr, strings.NewReader(string(buf)))
	req.SetBasicAuth(hs.username, hs.password)
	resp, err := hs.client.Do(req)
	if err != nil {
		return 0, 0, fmt.Errorf("curl %s err:%v", addr, err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return 0, 0, fmt.Errorf("unknow code %d", resp.StatusCode)
	}
	buf, errRead := ioutil.ReadAll(resp.Body)
	if errRead != nil {
		return 0, 0, fmt.Errorf("body read err:%v", errRead)
	}
	bufStr := strings.Replace(string(buf), "\n", "", -1)
	type ReplicRet struct {
		CurReplica    int `json:"curReplica"`
		DesireReplica int `json:"desireReplica"`
	}
	ret := ScaleRet{}
	errUnmarshal := json.Unmarshal([]byte(bufStr), &ret)
	if errUnmarshal != nil {
		return 0, 0, fmt.Errorf("Unmarshal json:%s err:%v", bufStr, errUnmarshal)
	}
	if ret.Code != 200 {
		return 0, 0, fmt.Errorf("get replic err:%v", ret.Err)
	}

	replicRet := ReplicRet{}
	errUnmarshal = json.Unmarshal([]byte(ret.Data), &replicRet)
	if errUnmarshal != nil {
		return 0, 0, fmt.Errorf("Unmarshal json:%s err:%v", ret.Data, errUnmarshal)
	}
	log.MyTagLogD(key, "GetCurReplica %s, %d,%d", ret.Data, replicRet.CurReplica, replicRet.DesireReplica)

	return replicRet.CurReplica, replicRet.DesireReplica, nil
}
func (hs *HttpScale) Clean(namespace string, sm *common.ScaleMoniter) (err error) {
	buf, jsonErr := json.Marshal(sm)
	if jsonErr != nil {
		return fmt.Errorf("Marshal ScaleMoniter %s err:%v", sm.Name, jsonErr)
	}
	addr := fmt.Sprintf("%s/clean", hs.scaleServerAddr)
	req, _ := http.NewRequest("POST", addr, strings.NewReader(string(buf)))
	req.SetBasicAuth(hs.username, hs.password)
	resp, err := hs.client.Do(req)
	if err != nil {
		return fmt.Errorf("curl %s err:%v", addr, err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unknow code %d", resp.StatusCode)
	}
	buf, errRead := ioutil.ReadAll(resp.Body)
	if errRead != nil {
		return fmt.Errorf("body read err:%v", errRead)
	}
	bufStr := strings.Replace(string(buf), "\n", "", -1)

	ret := ScaleRet{}
	errUnmarshal := json.Unmarshal([]byte(bufStr), &ret)
	if errUnmarshal != nil {
		return fmt.Errorf("Unmarshal json:%s err:%v", bufStr, errUnmarshal)
	}
	if ret.Code != 200 {
		return fmt.Errorf("clean err:%v", ret.Err)
	}
	return nil
}

func (hs *HttpScale) scale(namespace string, sm *common.ScaleMoniter, up bool, scaleTo int) error {
	_, wanted, errReplica := hs.GetCurReplica(namespace, sm)
	if errReplica != nil {
		return fmt.Errorf("GetCurReplica of %s err:%v", sm.Name, errReplica)
	}
	if up == true && wanted >= sm.MaxReplics {
		//return fmt.Errorf("cann't scaleup %s the cur replica is %d  >= MaxReplics:%d", sm.Name, wanted, sm.MaxReplics)
		return errors.NewScaleNotInRange(fmt.Sprintf("cann't scaleup %s the cur replica is %d  >= MaxReplics:%d", sm.Name, wanted, sm.MaxReplics))
	} else if up == false && wanted <= sm.MinReplics {
		return errors.NewScaleNotInRange(fmt.Sprintf("cann't scaledown %s the cur replica is %d <=  MinReplics:%d", sm.Name, wanted, sm.MinReplics))
	}

	buf, jsonErr := json.Marshal(sm)
	if jsonErr != nil {
		return fmt.Errorf("Marshal ScaleMoniter %s err:%v", sm.Name, jsonErr)
	}

	addr := fmt.Sprintf("%s/scale/0/%d", hs.scaleServerAddr, wanted-1)
	if up == true {
		addr = fmt.Sprintf("%s/scale/0/%d", hs.scaleServerAddr, wanted+1)
	}
	if scaleTo >= 0 {
		addr = fmt.Sprintf("%s/scale/0/%d", hs.scaleServerAddr, scaleTo)
	}

	req, _ := http.NewRequest("POST", addr, strings.NewReader(string(buf)))
	req.SetBasicAuth(hs.username, hs.password)
	resp, err := hs.client.Do(req)
	if err != nil {
		return fmt.Errorf("curl %s err:%v", addr, err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unknow code %d", resp.StatusCode)
	}
	buf, errRead := ioutil.ReadAll(resp.Body)
	if errRead != nil {
		return fmt.Errorf("read body err:%v", errRead)
	}
	bufStr := strings.Replace(string(buf), "\n", "", -1)
	ret := ScaleRet{}
	errUnmarshal := json.Unmarshal([]byte(bufStr), &ret)
	if errUnmarshal != nil {
		return fmt.Errorf("Unmarshal json:%s err:%v", string(buf), errUnmarshal)
	}
	if ret.Code != 200 {
		if strings.Contains(ret.Err, string(errors.ScaleDeleteMetricNotMatch)) == true {
			return errors.NewErrFromString(ret.Err)
		}
		return fmt.Errorf("scale err:%s", ret.Err)
	}
	return nil
}
func (hs *HttpScale) InitScale(namespace string, sm *common.ScaleMoniter) error {
	_, wanted, err := hs.GetCurReplica(namespace, sm)
	if err != nil {
		return fmt.Errorf("GetCurReplica of %s err:%v", sm.Name, err)
	}
	if wanted < sm.MinReplics {
		return hs.scale(namespace, sm, true, sm.MinReplics)
	} else if wanted > sm.MaxReplics {
		return hs.scale(namespace, sm, false, sm.MaxReplics)
	}
	return nil
}
func (hs *HttpScale) ScaleDown(namespace string, sm *common.ScaleMoniter) error {
	return hs.scale(namespace, sm, false, -1)
}
func (hs *HttpScale) ScaleUp(namespace string, sm *common.ScaleMoniter) error {
	return hs.scale(namespace, sm, true, -1)
}
