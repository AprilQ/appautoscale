package opentsdb

import (
	"appautoscale/common"
	//"appautoscale/log"
	"appautoscale/pkg/monitor/metricplugin"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"

	v1 "k8s.io/kubernetes/pkg/api/v1"
)

const (
	pluginName string = "opentsdb"
)

func ProbeVolumePlugins() []metricplugin.MetricPlugin {
	return []metricplugin.MetricPlugin{
		&opentsdb{},
	}
}

var _ metricplugin.MetricPlugin = &opentsdb{}
var _ metricplugin.MetricGeter = &opentsdbGeter{}

type opentsdb struct {
}

func (otb *opentsdb) PluginName() string {
	return pluginName
}

func (otb *opentsdb) Init() error {
	return nil
}

func (otb *opentsdb) NewMetricGeter(mg *common.MoniterMetric, pod *v1.Pod) (metricplugin.MetricGeter, error) {
	if mg.MetricGeter.OpenTsDBMetricGeter == nil {
		return nil, fmt.Errorf("MetricGeter.OpenTsDBMetricGeter == nil")
	}

	return &opentsdbGeter{
		client:              createHttpClient(),
		lowerLimit:          mg.DownScaleValue,
		upperLimit:          mg.UpScaleValue,
		openTsDBMetricGeter: mg.MetricGeter.OpenTsDBMetricGeter,
		pod:                 pod,
	}, nil
}

type opentsdbGeter struct {
	client              *http.Client
	lowerLimit          float64
	upperLimit          float64
	openTsDBMetricGeter *common.OpenTsDBMetricGeter
	pod                 *v1.Pod
}

type dataPoint struct {
	value float64
	time  int64
}

func (otbg *opentsdbGeter) IsUp(duration time.Duration, minMetrics int) (bool, error) {
	avg, err := otbg.GetLasterAvgValue(duration, minMetrics)
	if err != nil {
		return false, fmt.Errorf("GetLasterAvgValue err:%v", err)
	}
	return avg > otbg.upperLimit, nil
}

func (otbg *opentsdbGeter) IsDown(duration time.Duration, minMetrics int) (bool, error) {
	avg, err := otbg.GetLasterAvgValue(duration, minMetrics)
	if err != nil {
		return false, fmt.Errorf("GetLasterAvgValue err:%v", err)
	}
	return avg < otbg.lowerLimit, nil
}

func (otbg *opentsdbGeter) GetUpperLimit() float64 {
	return otbg.upperLimit
}

func (otbg *opentsdbGeter) GetLowerLimit() float64 {
	return otbg.lowerLimit
}

func (otbg *opentsdbGeter) GetCurrentValue() (float64, error) {
	values, err := otbg.getLasterValues(1 * time.Hour)
	if err != nil {
		return 0.0, fmt.Errorf("getLasterValues err:%v", err)
	}
	l := len(values)
	if l == 0 {
		return 0.0, fmt.Errorf("laster 1 hour has no metrics")
	}
	return values[l-1].value, nil
}

func (otbg *opentsdbGeter) GetLasterAvgValue(duration time.Duration, minMetrics int) (float64, error) {
	values, err := otbg.getLasterValues(duration)
	if err != nil {
		return 0.0, fmt.Errorf("getLasterValues err:%v", err)
	}
	l := len(values)
	if l < minMetrics {
		return 0.0, fmt.Errorf("last %v has only %d metrics < minMetrics:%d", duration, l, minMetrics)
	}
	var sum float64
	//debugValues := make([]float64, 0, len(values))
	for _, dp := range values {
		sum += dp.value
		//debugValues = append(debugValues, dp.value)
	}
	//log.MyLogD("%s metric:%s values:%v", otbg.pod.Name, otbg.openTsDBMetricGeter.Metric, debugValues)
	return sum / float64(l), nil
}

type subQuery struct {
	Aggregator string            `json:"aggregator"`
	Metric     string            `json:"metric"`
	Tags       map[string]string `json:"tags"`
}
type opentsdbQuery struct {
	Start   int64      `json:"start"`
	End     int64      `json:"end"`
	Queries []subQuery `json:"queries"`
}

func (otbg *opentsdbGeter) getTags() map[string]string {
	ret := make(map[string]string)
	for k, v := range otbg.openTsDBMetricGeter.Tags {
		if strings.Contains(v, "${podname}") {
			v = strings.Replace(v, "${podname}", otbg.pod.Name, -1)
		}
		ret[k] = v
	}
	return ret
}

func parseOpenTsDBRet(str string) ([]dataPoint, error) {
	index := strings.Index(str, "\"dps\":{\"")
	if index < 0 {
		return []dataPoint{}, fmt.Errorf("parse opentsdb ret err not find \"dps\":{\"")
	}
	tmpStr := str[index+9:]
	index = strings.Index(tmpStr, "}")
	if index < 0 {
		return []dataPoint{}, fmt.Errorf("parse opentsdb ret err not find }")
	}
	str = tmpStr[:index]
	//log.MyLogD("get dps:%s", str)
	strs := strings.Split(str, ",")
	ret := make([]dataPoint, 0, len(strs))
	for _, s := range strs {
		parts := strings.Split(s, ":")
		if len(parts) == 2 {
			timestr := strings.Replace(parts[0], "\"", "", -1)
			value, err1 := strconv.ParseFloat(parts[1], 64)
			timeSec, err2 := strconv.Atoi(timestr)
			if err1 == nil && err2 == nil {
				ret = append(ret, dataPoint{
					value: value,
					time:  int64(timeSec),
				})
			}
		}
	}
	return ret, nil
}
func (otbg *opentsdbGeter) getLasterValues(duration time.Duration) ([]dataPoint, error) {
	now := time.Now().Unix()
	query := opentsdbQuery{
		Start: now - int64(duration/time.Second),
		End:   now,
		Queries: []subQuery{
			subQuery{
				Aggregator: "sum",
				Metric:     otbg.openTsDBMetricGeter.Metric,
				Tags:       otbg.getTags(),
			},
		},
	}
	buf, err := json.Marshal(&query)
	if err != nil {
		return []dataPoint{}, err
	}
	resp, errPost := otbg.client.Post(otbg.openTsDBMetricGeter.Url, "", strings.NewReader(string(buf)))
	if errPost != nil {
		return []dataPoint{}, errPost
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return []dataPoint{}, fmt.Errorf("unknow status code :%d", resp.StatusCode)
	}
	buf, errRead := ioutil.ReadAll(resp.Body)
	if errRead != nil {
		return []dataPoint{}, errRead
	}
	return parseOpenTsDBRet(string(buf))
}

func createHttpClient() *http.Client {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		Dial: func(netw, addr string) (net.Conn, error) {
			conn, err := net.DialTimeout(netw, addr, 30*time.Second) //设置建立连接超时
			if err != nil {
				return nil, err
			}
			conn.SetDeadline(time.Now().Add(30 * time.Second)) //设置发送接受数据超时
			return conn, nil
		},
	}
	return &http.Client{
		Transport: tr,
	}
}
