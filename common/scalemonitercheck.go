package common

import (
	"appautoscale/common/triggerparse"
	"fmt"

	apimachineryvalidation "k8s.io/apimachinery/pkg/api/validation"
)

func nameCheck(name string) error {
	strs := apimachineryvalidation.NameIsDNSSubdomain(name, false)
	if len(strs) != 0 {
		return fmt.Errorf("name %s is invalidate", name)
	}
	return nil
}

func replicaCheck(sm *ScaleMoniter) error {
	if sm.MaxReplics < 0 {
		return fmt.Errorf("maxReplics %d is less than 0", sm.MaxReplics)
	}
	if sm.MinReplics < 0 {
		return fmt.Errorf("minReplics %d is less than 0", sm.MinReplics)
	}
	if sm.MaxReplics < sm.MinReplics {
		return fmt.Errorf("maxReplics %d is less than minReplics %d", sm.MaxReplics, sm.MinReplics)
	}
	return nil
}

func moniterMetricCheck(m *MoniterMetric) error {
	for _, b := range []byte(m.Name) {
		if (b < 'a' || b > 'z') && (b < 'A' || b > 'Z') {
			return fmt.Errorf("moniterMetric name %s, is invalidate, should belong to [a-zA-Z]", m.Name)
		}
	}
	if m.DownScaleValue >= m.UpScaleValue {
		return fmt.Errorf("moniterMetric %s  upScaleValue %d is less than downScaleValue %d", m.Name, m.UpScaleValue, m.DownScaleValue)
	}
	return nil
}

func moniterMetricsCheck(sm *ScaleMoniter) error {
	nameMap := make(map[string]bool)
	for _, mm := range sm.MoniterMetrics {
		if exist := nameMap[mm.Name]; exist == true {
			return fmt.Errorf("moniterMetric %s is Redefinition", mm.Name)
		}
		if err := moniterMetricCheck(&mm); err != nil {
			return err
		}
		nameMap[mm.Name] = true
	}
	return nil
}

func triggerCheck(trigger string, metrics []string) error {
	if trigger == "" {
		return nil
	}
	return triggerparse.TriggerStrValid(trigger, metrics)
}

func scaleTypeCheck(t ScaleType, template ScaleTemplate) error {
	switch t {
	case DeploymentReplica:
		if template.DeploymentReplica == nil {
			return fmt.Errorf("scaleType DeploymentReplica ScaleTemplate.DeploymentReplica should not be nil")
		} else {
			return nil
		}
	case ReplicaSetReplica:
		if template.ReplicaSetReplica == nil {
			return fmt.Errorf("scaleType ReplicaSetReplica ScaleTemplate.ReplicaSetReplica should not be nil")
		} else {
			return nil
		}
	case ReplicationControllerReplica:
		if template.ReplicationControllerReplica == nil {
			return fmt.Errorf("scaleType ReplicationControllerReplica ScaleTemplate.ReplicationControllerReplica should not be nil")
		} else {
			return nil
		}
	case DeploymentSet:
		if template.DeploymentSet == nil {
			return fmt.Errorf("scaleType DeploymentSet ScaleTemplate.DeploymentSet should not be nil")
		} else {
			return nil
		}
	case ReplicaSetSet:
		if template.ReplicaSetSet == nil {
			return fmt.Errorf("scaleType ReplicaSetSet ScaleTemplate.ReplicaSetSet should not be nil")
		} else {
			return nil
		}
	case ReplicationControllerSet:
		if template.ReplicationControllerSet == nil {
			return fmt.Errorf("scaleType ReplicationControllerSet ScaleTemplate.ReplicationControllerSet should not be nil")
		} else {
			return nil
		}
	}
	return fmt.Errorf("unknow scaleType %s", t)
}

func svcPoolCheck(sm *ScaleMoniter) error {
	if sm.ScaleType == DeploymentReplica || sm.ScaleType == ReplicationControllerReplica || sm.ScaleType == ReplicaSetReplica {
		if sm.SvcPool != nil {
			return fmt.Errorf("scaleType %s should not define svcPool", sm.ScaleType)
		}
	}
	if sm.SvcPool != nil && sm.SvcPool.Selector == nil && sm.SvcPool.SvcTemplate == nil {
		return fmt.Errorf("SvcPool please define SvcPool.Selector or SvcPool.SvcTemplate")
	}
	return nil
}

func getMetricsName(sm *ScaleMoniter) []string {
	ret := make([]string, 0, len(sm.MoniterMetrics))
	for _, m := range sm.MoniterMetrics {
		ret = append(ret, m.Name)
	}
	return ret
}

func ValidationScaleMoniter(sm *ScaleMoniter) error {
	if err := nameCheck(sm.Name); err != nil {
		return err
	}
	if err := replicaCheck(sm); err != nil {
		return err
	}
	if err := moniterMetricsCheck(sm); err != nil {
		return err
	}
	if err := triggerCheck(sm.ScaleUpTrigger, getMetricsName(sm)); err != nil {
		return fmt.Errorf("scaleUpTrigger %s is invalidate :%v", sm.ScaleUpTrigger, err)
	}
	if err := triggerCheck(sm.ScaleDownTrigger, getMetricsName(sm)); err != nil {
		return fmt.Errorf("scaleDownTrigger %s is invalidate :%v", sm.ScaleDownTrigger, err)
	}
	if err := triggerCheck(sm.CanDeleteTrigger, getMetricsName(sm)); err != nil {
		return fmt.Errorf("canDeleteTrigger %s is invalidate :%v", sm.CanDeleteTrigger, err)
	}
	if err := scaleTypeCheck(sm.ScaleType, sm.ScaleTemplate); err != nil {
		return err
	}
	return svcPoolCheck(sm)
}
