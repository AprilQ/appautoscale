package main

import (
	monitorapp "appautoscale/cmd/monitor/app"
	"appautoscale/log"
	pkgmonitor "appautoscale/pkg/monitor"
	"appautoscale/version"
	"os"

	"github.com/spf13/pflag"
)

func main() {
	mc := monitorapp.NewMonitorConfigg()
	mc.AddFlags(pflag.CommandLine)

	monitorapp.InitFlags()

	version.PrintAndExitIfRequested()

	log.SetDebug(mc.Debug)
	if err := pkgmonitor.Run(mc); err != nil {
		log.MyLogE("error: %v\n", err)
		os.Exit(1)
	}
}
