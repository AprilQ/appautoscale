package main

import (
	scalemanagerapp "appautoscale/cmd/scalemanager/app"
	"appautoscale/log"
	pkgscalemanager "appautoscale/pkg/scalemanager"
	"appautoscale/version"
	"os"

	"github.com/spf13/pflag"
)

func main() {
	sma := scalemanagerapp.NewScaleManagerConfig()
	sma.AddFlags(pflag.CommandLine)

	scalemanagerapp.InitFlags()

	version.PrintAndExitIfRequested()

	log.SetDebug(sma.Debug)
	if err := pkgscalemanager.Run(sma); err != nil {
		log.MyLogE("error: %v\n", err)
		os.Exit(1)
	}
}
