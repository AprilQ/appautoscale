package errors

import (
	"fmt"
	"strings"
)

type StatusReason string

const (
	ScaleNotInRange           StatusReason = "scaleNotInRange"
	ScaleDeleteMetricNotMatch StatusReason = "scaleDeleteMetricNotMatch"
	ScaleUnknow               StatusReason = "unknow"
)

type ScaleError struct {
	reason StatusReason
	msg    string
}

func (se *ScaleError) Error() string {
	return fmt.Sprintf("%s:%s", se.reason, se.msg)
}

func NewErrFromString(str string) error {
	pos := strings.Index(str, ":")
	if pos > 0 {
		reason := str[:pos]
		switch StatusReason(reason) {
		case ScaleNotInRange:
			return NewScaleNotInRange(str[pos+1:])
		case ScaleDeleteMetricNotMatch:
			return NewScaleDeleteMetricNotMatch(str[pos+1:])
		}
	}
	return fmt.Errorf("%s", str)
}

func NewScaleNotInRange(msg string) error {
	return &ScaleError{reason: ScaleNotInRange, msg: msg}
}

func NewScaleDeleteMetricNotMatch(msg string) error {
	return &ScaleError{reason: ScaleDeleteMetricNotMatch, msg: msg}
}

func IsScaleNotInRange(err error) bool {
	return reasonForError(err) == ScaleNotInRange
}

func IsScaleDeleteMetricNotMatch(err error) bool {
	return reasonForError(err) == ScaleDeleteMetricNotMatch
}

func reasonForError(err error) StatusReason {
	if t, ok := err.(*ScaleError); ok {
		return t.reason
	} else {
		return ScaleUnknow
	}
}
